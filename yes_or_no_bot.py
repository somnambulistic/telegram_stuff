""" the first bot """

import logging
import requests
import ujson

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = '986363922:AAH57QX4WbZ9DFpuKmYTNE-vVwZUZdET75U'

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
BOT = Bot(token=API_TOKEN)
DP = Dispatcher(BOT)

@DP.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends '/start' or '/help' command
    """

    await message.reply("Hi!\nI'm EchoBot!\nPowered by aiogram.")


@DP.message_handler(commands=['choose'])
async def make_decision(message: types.Message):
    """ yes or no """
    url = requests.get('https://yesno.wtf/api').text
    urljson = ujson.loads(url)

    await message.reply_animation(caption=urljson['answer'], animation=urljson['image'])

@DP.message_handler(commands=['ron'])
async def ron_swanson(message: types.Message):
    """ Ron Swanson """
    url = requests.get('http://ron-swanson-quotes.herokuapp.com/v2/quotes').text
    urljson = ujson.loads(url)

    await message.reply(urljson[0])


if __name__ == '__main__':
    executor.start_polling(DP, skip_updates=True)
